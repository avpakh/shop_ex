# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cookiecart', '0005_auto_20150920_1003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.CharField(default='\u043d\u0435 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d', max_length=20, choices=[('\u043d\u0435 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d', '\u043d\u0435 \u043e\u0442\u043f\u0440\u0430\u0432\u043b\u0435\u043d'), ('\u043e\u0436\u0438\u0434\u0430\u043d\u0438\u0435', '\u043e\u0436\u0438\u0434\u0430\u043d\u0438\u0435'), ('\u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0430', '\u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0430'), ('\u043e\u0442\u043c\u0435\u043d\u0435\u043d \u043c\u0435\u043d\u0435\u0434\u0436\u0435\u0440\u043e\u043c', '\u043e\u0442\u043c\u0435\u043d\u0435\u043d \u043c\u0435\u043d\u0435\u0434\u0436\u0435\u0440\u043e\u043c'), ('\u043e\u0442\u043c\u0435\u043d\u0435\u043d \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u043c', '\u043e\u0442\u043c\u0435\u043d\u0435\u043d \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0435\u043c'), ('\u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d', '\u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d')]),
        ),
        migrations.AlterField(
            model_name='order',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 20, 13, 46, 41, 602417)),
        ),
    ]
