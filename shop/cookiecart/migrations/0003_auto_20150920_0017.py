# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cookiecart', '0002_auto_20150919_2359'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='cart_user',
            new_name='cartuser',
        ),
        migrations.AlterField(
            model_name='order',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 20, 0, 17, 49, 176319)),
        ),
    ]
