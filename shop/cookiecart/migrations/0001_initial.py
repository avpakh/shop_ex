# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('secret_key', models.CharField(max_length=48)),
            ],
        ),
        migrations.CreateModel(
            name='CartUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Good',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ammount', models.IntegerField(default=1)),
                ('fixed_price', models.DecimalField(max_digits=100, decimal_places=2)),
                ('good_type', models.ForeignKey(to='products.Product')),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(default=datetime.datetime(2015, 9, 19, 21, 13, 22, 729599))),
                ('status', models.CharField(default=b'unsubmited', max_length=20, choices=[(b'unsubmited', b'unsubmited'), (b'waiting', b'waiting'), (b'processing', b'processing'), (b'cancelled by manager', b'cancelled by manager'), (b'cancelled by user', b'cancelled by user'), (b'completed', b'completed')])),
                ('self_delivery', models.BooleanField(default=False)),
                ('delivery_adress', models.CharField(max_length=100, null=True)),
                ('delivery_time', models.CharField(max_length=50, null=True)),
                ('contact_phone', models.CharField(max_length=15, null=True)),
                ('cart', models.ForeignKey(to='cookiecart.Cart')),
                ('cart_user', models.ForeignKey(to='cookiecart.CartUser', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='good',
            name='order',
            field=models.ForeignKey(to='cookiecart.Order'),
        ),
    ]
