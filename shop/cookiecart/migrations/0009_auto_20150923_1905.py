# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cookiecart', '0008_auto_20150920_1854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='good',
            name='fixed_price',
            field=models.DecimalField(null=True, max_digits=100, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='order',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 23, 19, 5, 8, 90334)),
        ),
    ]
