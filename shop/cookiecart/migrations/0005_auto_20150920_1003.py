# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0001_initial'),
        ('cookiecart', '0004_auto_20150920_0051'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='manageruser',
            field=models.ForeignKey(to='management.ManagerUser', null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 20, 10, 3, 28, 45271)),
        ),
    ]
