from django.shortcuts import render
from django.http import HttpResponse

from .models import Orderslist


def get_orderslist(request):
    if request.user.is_authenticated():
        try:
            orderslist = Orderslist.objects.get(user=request.user)
        except Orderslist.DoesNotExist:
            orderslist = Orderslist(user=request.user)
            orderslist.save()
    else:
        orderslist = None
    return orderslist

def index(request):
    orderslist = get_orderslist(request)
    context = {'orderslist': orderslist, 'orders': orderslist.order_set.all()}
    return render(request, 'orderslist/index.html', context)
