# -*- coding: utf-8 -*-

#from djorm_pgfulltext.models import SearchManager
#from djorm_pgfulltext.fields import VectorField
from django.db import models
from sorl.thumbnail import ImageField
from django.contrib.sessions.models import Session
from django.utils import timezone

from accounts.models import AuthUser




# Create your models here.


class ProductCategory(models.Model):
    category_product_id = models.AutoField(primary_key=True)
    catslug = models.SlugField()
    category_product_name = models.CharField("Category_name", default=' none ', max_length=50)
    category_description = models.CharField("Category_description", default=' none ', max_length=200)

    class Meta:
        db_table = u'Categories'

    def __unicode__(self):
        return u" %s " % (self.category_product_name)


class ProductLevCategory(models.Model):
    levcategory_product_id = models.AutoField(primary_key=True)
    levcategory_product_name = models.CharField("SubCategory_name", default=' none ', max_length=50)
    levslug = models.SlugField()
    levcategory_description = models.CharField('SubCategory_desc', default=' none ', max_length=200)
    product_category = models.ForeignKey(ProductCategory)

    class Meta:
        db_table = u'LevCategories'

    def __unicode__(self):
        return u" %s " % (self.levcategory_product_name)

    @staticmethod
    def amount(self,products):
        amount=0
        for product in products:
            if product.product_levcategory == self:
                print amount
                amount=amount+1
        return amount


class ProductTags(models.Model):
    product_tags_id = models.AutoField(primary_key=True)
    product_tags_text = models.CharField("Tags_text", default=' ', max_length=50)

    class Meta:
        db_table = u'Tags'

    def __unicode__(self):
        return u" %s " % (self.product_tags_text)


class ProductRating(models.Model):
    product_rating_id = models.AutoField(primary_key=True)
    product_rating = models.FloatField("Rating of products", default='0')

    class Meta:
        db_table = u'Rating'

    def __unicode__(self):
        return u" %s " % (self.product_rating)


class ProducerProduct(models.Model):
    producer_product_id = models.AutoField(primary_key=True)
    producer_product_name = models.CharField("Producer_name", default='Purina', max_length=50)

    class Meta:
        db_table = u'Producers'

    def __unicode__(self):
        return u" %s " % (self.producer_product_name)


class Product(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField(null=True, blank=True)
    adddescription = models.TextField(null=True, blank=True)
    price = models.DecimalField(decimal_places=2, max_digits=100, default=29.99)
    sale_price = models.DecimalField(decimal_places=2, max_digits=100,default=0)

    slug = models.SlugField()
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    active = models.BooleanField(default=True)
    product_producer = models.ForeignKey(ProducerProduct)
    product_levcategory = models.ForeignKey(ProductLevCategory)
    product_tags = models.ForeignKey(ProductTags)
    product_rating = models.DecimalField(decimal_places=2, max_digits=3,default=0)
    product_vote = models.DecimalField(decimal_places=0,max_digits=3, default=0)
    urllink = models.URLField(blank=True)
    product_image= ImageField(upload_to='products/static/im/')

    created_at = models.DateTimeField("When_created", auto_now_add=True, auto_now=False)
    updated_at = models.DateTimeField("When _updated", auto_now_add=False, auto_now=True)


    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-created_at']
        unique_together = ('title', 'slug')

    def get_price(self):
        return self.price






class Review(models.Model):
    user = models.OneToOneField(AuthUser, null=True)
    session = models.OneToOneField(Session, null=True)

    def __unicode__(self):
        if self.user:
            return u'{} review'.format(self.user)
        elif self.session:
            return u'{} review'.format(self.session.pk)
        else:
            return None


class ProductComments(models.Model):
    review = models.ForeignKey(Review)
    product_comments_id = models.AutoField(primary_key=True, unique=True)
    product_comments_text = models.TextField("Product_comments_text")  # possible widget if any
    auth_username = models.ForeignKey(AuthUser)
    idprod = models.ForeignKey(Product)  #link to existing id in Product

    class Meta:
        db_table = u'Comments'

    def __unicode__(self):
        return "%s %s " % (self.pk, self.product_comments_text)


    def __str__(self):
        return "%s %s " % (self.pk, self.product_comments_text)
