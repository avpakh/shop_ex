from django import template

register = template.Library()

@register.inclusion_tag('base.html',takes_context=False)
def get_amount(levcat,products):
    amount=0
    for product in products:
        if product.product_levcategory == levcat:
            amount=amount+1
    get_amount=amount
    return {'get_amount':get_amount}
