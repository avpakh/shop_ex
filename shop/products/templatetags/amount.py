from django import template

register = template.Library()

@register.simple_tag()
def show_amount(levcat):
    return  levcat.product_set.count

@register.simple_tag()
def get_amount(levcat,products):
    amount=0
    for product in products:
        if product.product_levcategory == levcat:
            amount=amount+1
    get_amount=amount
    print get_amount
    return  get_amount

@register.simple_tag()
def get_brandamount(brand,products):
    amount=0
    for product in products:
        if product.product_producer == brand:
            amount=amount+1
    get_brandamount=amount
    print get_brandamount
    return  get_brandamount

@register.filter
def index(sequence, position):
    return sequence[position]


