# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_auto_20150914_1259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producerproduct',
            name='producer_product_name',
            field=models.CharField(default=b'Purina', max_length=50, verbose_name=b'Producer_name'),
        ),
    ]
