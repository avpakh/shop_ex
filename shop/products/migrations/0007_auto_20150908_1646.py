# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0006_auto_20150906_1940'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='product_vote',
            field=models.DecimalField(default=0, max_digits=3, decimal_places=0),
        ),
    ]
