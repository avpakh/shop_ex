# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_product_product_vote'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='product_vote',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=0),
        ),
    ]
