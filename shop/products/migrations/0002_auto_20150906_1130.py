# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProducerProduct',
            fields=[
                ('producer_product_id', models.AutoField(serialize=False, primary_key=True)),
                ('producer_product_name', models.CharField(default=b'Purina ', max_length=50, verbose_name=b'Producer_name')),
            ],
            options={
                'db_table': 'Producers',
            },
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('category_product_id', models.AutoField(serialize=False, primary_key=True)),
                ('category_product_name', models.CharField(default=b' none ', max_length=50, verbose_name=b'Category_name')),
                ('category_description', models.CharField(default=b' none ', max_length=200, verbose_name=b'Category_description')),
            ],
            options={
                'db_table': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='ProductComments',
            fields=[
                ('product_comments_id', models.AutoField(unique=True, serialize=False, primary_key=True)),
                ('product_comments_text', models.CharField(max_length=255, verbose_name=b'Product_comments_text')),
            ],
            options={
                'db_table': 'Comments',
            },
        ),
        migrations.CreateModel(
            name='ProductLevCategory',
            fields=[
                ('levcategory_product_id', models.AutoField(serialize=False, primary_key=True)),
                ('levcategory_product_name', models.CharField(default=b' none ', max_length=50, verbose_name=b'SubCategory_name')),
                ('levcategory_description', models.CharField(default=b' none ', max_length=200, verbose_name=b'SubCategory_desc')),
                ('product_category', models.ForeignKey(to='products.ProductCategory')),
            ],
            options={
                'db_table': 'LevCategories',
            },
        ),
        migrations.CreateModel(
            name='ProductRating',
            fields=[
                ('product_rating_id', models.AutoField(serialize=False, primary_key=True)),
                ('product_rating', models.CharField(default=b'0', max_length=5, verbose_name=b'Rating of products', choices=[(b'*****', b'*****'), (b'****', b'****'), (b'***', b'***'), (b'**', b'**'), (b'*', b'*'), (b'-', b' - ')])),
            ],
            options={
                'db_table': 'Rating',
            },
        ),
        migrations.CreateModel(
            name='ProductTags',
            fields=[
                ('product_tags_id', models.AutoField(serialize=False, primary_key=True)),
                ('product_tags_text', models.CharField(default=b' ', max_length=50, verbose_name=b'Tags_text')),
            ],
            options={
                'db_table': 'Tags',
            },
        ),
        migrations.AddField(
            model_name='product',
            name='urllink',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='productcomments',
            name='id_product',
            field=models.ForeignKey(to='products.Product'),
        ),
        migrations.AddField(
            model_name='product',
            name='product_levcategory',
            field=models.ForeignKey(default=1, to='products.ProductLevCategory'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='product_producer',
            field=models.ForeignKey(default=1, to='products.ProducerProduct'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='product_rating',
            field=models.ForeignKey(default=1, to='products.ProductRating'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='product_tags',
            field=models.ForeignKey(default=1, to='products.ProductTags'),
            preserve_default=False,
        ),
    ]
