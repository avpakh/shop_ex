# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20150906_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productrating',
            name='product_rating',
            field=models.FloatField(default=b'0', verbose_name=b'Rating of products'),
        ),
    ]
