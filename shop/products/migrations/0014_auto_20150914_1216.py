# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0013_auto_20150914_1129'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='image',
        ),
        migrations.AddField(
            model_name='product',
            name='product_image',
            field=sorl.thumbnail.fields.ImageField(default=1, upload_to=b'product/images'),
            preserve_default=False,
        ),
    ]
