# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_auto_20150908_1646'),
    ]

    operations = [
        migrations.AddField(
            model_name='productlevcategory',
            name='levslug',
            field=models.SlugField(default=1),
            preserve_default=False,
        ),
    ]
