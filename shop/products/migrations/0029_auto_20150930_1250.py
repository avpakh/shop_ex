# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_auto_20150930_0812'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['-created_at']},
        ),
        migrations.AddField(
            model_name='product',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 12, 50, 33, 920215), verbose_name=b'When_created', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 30, 12, 50, 46, 41900), verbose_name=b'When _updated', auto_now=True),
            preserve_default=False,
        ),
    ]
