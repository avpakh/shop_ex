# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0011_auto_20150914_1006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'/dev/null'),
        ),
    ]
