# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0020_auto_20150918_0831'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='adddescription',
            field=models.TextField(null=True, blank=True),
        ),
    ]
