# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0004_auto_20150906_1924'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_vote',
            field=models.IntegerField(null=True),
        ),
    ]
