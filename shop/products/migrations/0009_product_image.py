# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_productlevcategory_levslug'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='image',
            field=sorl.thumbnail.fields.ImageField(default=1, upload_to=b'whatever'),
            preserve_default=False,
        ),
    ]
