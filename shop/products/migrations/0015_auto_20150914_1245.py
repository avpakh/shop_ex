# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0014_auto_20150914_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='product_image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'im/'),
        ),
    ]
