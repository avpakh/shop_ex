# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0024_productcomments_idprod'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productcomments',
            name='idprod',
            field=models.ForeignKey(to='products.Product', null=True),
        ),
    ]
