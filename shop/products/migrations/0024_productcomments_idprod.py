# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0023_auto_20150918_1626'),
    ]

    operations = [
        migrations.AddField(
            model_name='productcomments',
            name='idprod',
            field=models.OneToOneField(null=True, to='products.Product'),
        ),
    ]
