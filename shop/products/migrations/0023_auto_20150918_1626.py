# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('products', '0022_auto_20150918_1330'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productcomments',
            name='id_product',
        ),
        migrations.AddField(
            model_name='productcomments',
            name='auth_username',
            field=models.ForeignKey(default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='productcomments',
            name='review',
            field=models.ForeignKey(default=1, to='products.Review'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='product',
            name='product_rating',
            field=models.DecimalField(default=0, max_digits=3, decimal_places=2),
        ),
    ]
