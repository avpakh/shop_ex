# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0025_auto_20150918_1950'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productcomments',
            name='idprod',
            field=models.ForeignKey(default=1, to='products.Product'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='productcomments',
            name='product_comments_text',
            field=models.TextField(verbose_name=b'Product_comments_text'),
        ),
    ]
