from django.conf.urls import include, url
from . import views



urlpatterns = [
        url(r'^$','products.views.mains'),
        url(r'^all/$', 'products.views.all', name='products'),
        url(r'^products/', include('products.urls', namespace="products")),
        url(r'^(?P<pk>[0-9]+)/$', 'products.views.showdetail', name='detail'),
        url(r'^(?P<product_id>[0-9]+)/vote/$', 'products.views.rating', name='vote'),
        url(r'^(?P<catslug>.+)/$', 'products.views.getLevItems', name='listcat'),
        url(r'^(?P<levslug>.+)/$', 'products.views.getItems', name='list'),
        url(r'^(?P<brand>.+)/$', 'products.views.getBrandItems'),
        url(r'^(?P<catslug>.+)/(?P<levslug>.+)/$', 'products.views.getItems'),
        url(r'^search/$', views.SearchView.as_view()),
       ]

