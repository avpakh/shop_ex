# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.views import generic
from django.http import HttpResponseRedirect
from django.contrib.sessions.models import Session

from django.core.urlresolvers import reverse
from django.db.models import Q
from decimal import Decimal
from datetime import datetime


# Create your views here.

from cookiecart.models import Cart
from cookiecart.models import Order
from cookiecart.models import Good
from cookiecart.models import CartUser
from .models import Review
from .models import Product
from .models import ProductCategory
from .models import ProductLevCategory
from .models import ProductComments
from .models import ProducerProduct

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

def get_cart(request):
    if request.COOKIES.has_key('petsshopcart'):
        secret_key = request.COOKIES['petsshopcart']
        try:
            cart = Cart.objects.get(secret_key=secret_key)
        except Cart.DoesNotExist:
            cart = None
    else:
        cart = None
    return cart


def get_cartuser(request):
    if request.user.is_authenticated():
        try:
            cartuser = CartUser.objects.get(user=request.user)
        except CartUser.DoesNotExist:
            cartuser = CartUser(user=request.user)
            cartuser.save()
    else:
        return None
    return cartuser


def get_cart_old(request):
    if request.user.is_authenticated():
        # Provide using user-based cart for authenticated users
        try:
            cart = Cart.objects.get(user=request.user)
        except Cart.DoesNotExist:
            cart = Cart(user=request.user)
            cart.save()
    else:
        # Provide using session-based cart for nonauthenticated users
        request.session.save()
        current_session = Session.objects.get(pk = request.session.session_key)
        try:
            cart = Cart.objects.get(session=current_session)
        except Cart.DoesNotExist:
            cart = Cart(session=current_session)
            cart.save()
    return cart

def get_good (request):

    cart = get_cart(request)

    try:
        order = Order.objects.get(cart=cart, status=u'не отправлен')
        #order = Order.objects.get(cart=cart, status='unsubmited')
    except Order.DoesNotExist:
        order = '0'
        good = '0'
        return '0'
    if order:
            try:
                good = order.good_set.count()
            except Good.DoesNotExist:
                good = '0'
                return '0'
            if good:
                return good

def get_totalprice(request):
    total = 0

    cart = get_cart(request)

    try:
        order = Order.objects.get(cart=cart, status=u'не отправлен')
        #order = Order.objects.get(cart=cart, status='unsubmited')
    except Order.DoesNotExist:
        order = '0'
        total = '0'
        return '0'
    if order:
            try:
                good = order.good_set.count()
                for good in order.good_set.all():
                    if good.fixed_price:
                        total += good.fixed_price*good.ammount
                    elif good.good_type.sale_price:
                        total += good.good_type.sale_price*good.ammount
                    else:
                        total += good.good_type.price*good.ammount
                return total


            except Good.DoesNotExist:
                good = '0'
                return '0'
                if good:
                    return total


def index_view(request):
    template_name = "basein.html"

    new_products = Product.objects.all().order_by('-created_at')[:6]
    best_products = Product.objects.all().order_by('-product_rating')[:6]
    sale_products = Product.objects.all()
    result = {}
    for product in sale_products:
        if product.price > product.sale_price and product.sale_price !=0:

            prod_price = Decimal(product.price)
            prod_sale = Decimal(product.sale_price)

            diff = Decimal(prod_price - prod_sale)

            result[product.id] = diff

    result = sorted(result.items(), key=lambda (k, v): v,reverse=True)
    sale_products = []
    for key,value in result:
        p = Product.objects.get(id = key)
        sale_products.append(p)

    context = {'new_products': new_products, 'best_products': best_products, 'sale_products':sale_products}
    return render(request, template_name, context)

def mains(request):

    categorys = ProductCategory.objects.all()

    subcategorys = ProductLevCategory.objects.all().select_related('product_category')

    products = Product.objects.all().select_related('product_levcategory').order_by('product_levcategory')

    products_list = Product.objects.all().order_by('product_levcategory')

    paginator = Paginator(products_list, 9)

    page = request.GET.get('page')

    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)

    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)

    vars = dict(
            page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,
            cartm = get_good(request),carta=get_totalprice(request),filter='not',)

    return render(request,'/basein.html',context=vars)



def all(request):

    categorys = ProductCategory.objects.all()

    subcategorys = ProductLevCategory.objects.all().select_related('product_category')

    products = Product.objects.all().select_related('product_levcategory').order_by('product_levcategory')

    products_list = Product.objects.all().order_by('product_levcategory')

    paginator = Paginator(products_list, 9)

    page = request.GET.get('page')

    new_product = Product.objects.all().order_by('-created_at')[:6]


    sale_products = Product.objects.all()
    result = {}
    for product in sale_products:
        if product.price > product.sale_price and product.sale_price !=0:

            prod_price = Decimal(product.price)
            prod_sale = Decimal(product.sale_price)

            diff = Decimal(prod_price - prod_sale)

            result[product.id] = diff

    result = sorted(result.items(), key=lambda (k, v): v,reverse=True)
    sale_products = []
    for key,value in result:
        p = Product.objects.get(id = key)
        sale_products.append(p)
    print sale_products

    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)

    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)

    vars = dict(
            page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,
            sublev='Catalog',cartm = get_good(request),carta=get_totalprice(request),filter='not',top='ok',new_products=new_product,sale_products=sale_products)

    return render_to_response('products/all.html', vars, context_instance=RequestContext(request))

class ListcatView(generic.DetailView):
    model = Product
    template_name = 'products/listcat.html'


def getLevItems (request,catslug):

    var_slug=catslug

    print var_slug

    slug = var_slug.split('/')

    if len(slug)>1:

        ttt= slug[1] # level search

        if ttt == 'search':

            qqq= slug[2]

            cat_obj = get_object_or_404(ProductCategory, catslug=slug[0])

            maincategory = cat_obj

            categorys = ProductCategory.objects.all().filter(catslug=slug[0])

            subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

            products = Product.objects.filter(product_levcategory=subcategorys).filter(Q(title__icontains=unicode(qqq)) | Q(description__icontains=unicode(qqq)))

            brands = []
            brands_lev = []
            zzz= {}

            for kk in products:
                if kk.product_producer in brands:
                    pass
                else:
                    brands.append(kk.product_producer)
                    brands_lev.append(kk.product_levcategory.levslug)

            products_list = products

            print products,subcategorys

            paginator = Paginator(products_list, 9)

            page = request.GET.get('page')

            try:
                page_obj = paginator.page(page)
            except PageNotAnInteger:
                page_obj = paginator.page(1)
            except EmptyPage:
                page_obj = paginator.page(paginator.num_pages)
            vars = dict(
                 page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,filter='search',brands=brands,brandlev=brands_lev,
                 mainlev=maincategory,cartm= get_good(request),carta=get_totalprice(request),sublev='No results',querysea=qqq)
            return render_to_response('products/search_form.html', vars, context_instance=RequestContext(request))


        if ttt!='search':

            var_slug=str(catslug)

            slug = var_slug.split('/')

            levslug = slug[0]

            brandslug = slug[1]

            lev_obj = get_object_or_404(ProductLevCategory, levslug=levslug)

            producer_obj = get_object_or_404(ProducerProduct, producer_product_name=brandslug)

            maincategory = lev_obj.product_category

            levelcategory = lev_obj.levcategory_product_name

            categorys = ProductCategory.objects.all().filter(catslug=maincategory.catslug)

            subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

            products = Product.objects.all().filter(product_levcategory=lev_obj).filter(product_producer=producer_obj).order_by('product_levcategory')

            brands = []

            for kk in products:
                if kk.product_producer in brands:
                    pass
                else:
                    brands.append(kk.product_producer)
                    print kk , kk.product_producer, brands


            products_list = products

            paginator = Paginator(products_list, 9)

            page = request.GET.get('page')

            try:
                page_obj = paginator.page(page)
            except PageNotAnInteger:
                page_obj = paginator.page(1)
            except EmptyPage:
                page_obj = paginator.page(paginator.num_pages)
            vars = dict(
                page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,filter='active',brands=brands,
                sublev=levelcategory, brand=brandslug ,mainlev=maincategory,mainsublev= lev_obj,cartm= get_good(request),carta=get_totalprice(request))
            return render_to_response('products/listcat.html', vars, context_instance=RequestContext(request))


    else:

        categorys = ProductCategory.objects.all().filter(catslug=catslug)

        if len(categorys) != 0:

            cat_obj = get_object_or_404(ProductCategory, catslug=catslug)

            maincategory = cat_obj

            categorys = ProductCategory.objects.all().filter(catslug=catslug)

            subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

            products = Product.objects.all().filter(product_levcategory=subcategorys).order_by('product_levcategory')

            products_list = products

            paginator = Paginator(products_list, 9)

            page = request.GET.get('page')

            try:
                page_obj = paginator.page(page)
            except PageNotAnInteger:
                page_obj = paginator.page(1)
            except EmptyPage:
                page_obj = paginator.page(paginator.num_pages)
            vars = dict(
                page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,
                mainlev=maincategory,cartm= get_good(request),carta=get_totalprice(request),filter='not', )
            return render_to_response('products/listcat.html', vars, context_instance=RequestContext(request))

        else:

            subcategorys = ProductLevCategory.objects.all().filter(levslug=catslug)

            if len(subcategorys) !=0:

                lev_obj = get_object_or_404(ProductLevCategory, levslug=catslug)

                maincategory = lev_obj.product_category

                levelcategory = lev_obj.levcategory_product_name

                categorys = ProductCategory.objects.all().filter(catslug=maincategory.catslug)

                subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

                print maincategory,subcategorys

                products = Product.objects.all().filter(product_levcategory=lev_obj).order_by('product_levcategory')

                brands = []

                for kk in products:
                    if kk.product_producer in brands:
                        pass
                    else:
                        brands.append(kk.product_producer)
                        print kk , kk.product_producer, brands

                products_list = products

                paginator = Paginator(products_list, 9)

                page = request.GET.get('page')

                try:
                    page_obj = paginator.page(page)
                except PageNotAnInteger:
                    page_obj = paginator.page(1)
                except EmptyPage:
                    page_obj = paginator.page(paginator.num_pages)
                vars = dict(
                    page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,brands = brands,filter='ok',
                    sublev=levelcategory, mainlev=maincategory,mainsublev= lev_obj,cartm= get_good(request),carta=get_totalprice(request))
                return render_to_response('products/listcat.html', vars, context_instance=RequestContext(request))

            else:



                if  ('q' in request.GET) and request.GET['q'].strip():

                        q = request.GET['q']

                        sea = Product.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))

                        newlevcat = []

                        newcat= []

                        for sea_items in sea:
                            if sea_items.product_levcategory not in newlevcat:
                                newlevcat.append(sea_items.product_levcategory)
                                newcat.append(sea_items.product_levcategory.product_category)

                        rrr = sea.only('product_levcategory').filter(Q(title__icontains=q) | Q(description__icontains=q)).order_by('product_levcategory')

                        mlev=[]

                        for yyy in rrr:
                            if yyy.product_levcategory not in mlev:
                                mlev.append(yyy.product_levcategory)


                        products_list = sea

                        paginator = Paginator(products_list, 9)

                        page = request.GET.get('page')

                        try:
                            page_obj = paginator.page(page)
                        except PageNotAnInteger:
                            page_obj = paginator.page(1)
                        except EmptyPage:
                            page_obj = paginator.page(paginator.num_pages)
                        vars = dict(
                            page_obj=page_obj,subcategorys=mlev,categorys= newcat,items=sea,products=rrr,
                            querysea=q,sublev='No results',cartm =get_good(request),carta=get_totalprice(request)
                            )

                        return render_to_response('products/search_form.html', vars, context_instance=RequestContext(request))


class BrandItemsView(generic.DetailView):
    model = Product
    template_name = 'products/brand.html'

def getBrandItems(request):

    categorys = ProductCategory.objects.all().filter(catslug=catslug)

    if len(categorys) != 0:

        cat_obj = get_object_or_404(ProductCategory, catslug=catslug)

        print cat_obj

        maincategory = cat_obj

        categorys = ProductCategory.objects.all().filter(catslug=catslug)

        subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

        products = Product.objects.all().filter(product_levcategory=subcategorys).order_by('product_levcategory')

        products_list = products

        paginator = Paginator(products_list, 9)

        page = request.GET.get('page')

        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        vars = dict(
            page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,
            mainlev=maincategory,cartm= get_good(request),carta=get_totalprice(request),filter='not', )
        return render_to_response('products/listcat.html', vars, context_instance=RequestContext(request))

    else:

        subcategorys = ProductLevCategory.objects.all().filter(levslug=catslug)

        if len(subcategorys) !=0:

            lev_obj = get_object_or_404(ProductLevCategory, levslug=catslug)

            maincategory = lev_obj.product_category

            levelcategory = lev_obj.levcategory_product_name

            categorys = ProductCategory.objects.all().filter(catslug=maincategory.catslug)

            subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

            print maincategory,subcategorys

            products = Product.objects.all().filter(product_levcategory=lev_obj).order_by('product_levcategory')

            brands = []

            for kk in products:
                if kk.product_producer in brands:
                    pass
                else:
                    brands.append(kk.product_producer)
                    print kk , kk.product_producer, brands

            products_list = products

            paginator = Paginator(products_list, 9)

            page = request.GET.get('page')

            try:
                page_obj = paginator.page(page)
            except PageNotAnInteger:
                page_obj = paginator.page(1)
            except EmptyPage:
                page_obj = paginator.page(paginator.num_pages)
            vars = dict(
                page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=products,products=products,brands = brands,filter='ok',
                sublev=levelcategory, mainlev=maincategory,mainsublev= lev_obj,cartm= get_good(request),carta=get_totalprice(request))
            return render_to_response('products/listcat.html', vars, context_instance=RequestContext(request))







class ListobjView(generic.DetailView):
    model = Product
    template_name = 'products/list.html'


def getItems (request,catslug,levslug):

   if levslug != "search" :

        cat_obj = get_object_or_404(ProductCategory, catslug=catslug)

        maincategory = cat_obj

        categorys = ProductCategory.objects.all().filter(catslug=catslug)

        z = get_object_or_404(ProductLevCategory, levslug=levslug)

        items = Product.objects.filter(product_levcategory=z)

        levelcategory = z.levcategory_product_name

        subcategorys = ProductLevCategory.objects.all().filter(product_category=categorys)

        products = Product.objects.all().select_related('product_levcategory').order_by('product_levcategory')

        products_list = items

        paginator = Paginator(products_list, 9)

        page = request.GET.get('page')

        try:
            page_obj = paginator.page(page)
        except PageNotAnInteger:
            page_obj = paginator.page(1)
        except EmptyPage:
            page_obj = paginator.page(paginator.num_pages)
        vars = dict(
            page_obj=page_obj,subcategorys=subcategorys,categorys= categorys,items=items,products=products,
            sublev=levelcategory,mainlev=maincategory,cartm= get_good(request),carta=get_totalprice(request))

        return render_to_response('products/list.html', vars, context_instance=RequestContext(request))

        # return render(request, 'products/list.html', context)
   else:

        if  ('q' in request.GET) and request.GET['q'].strip():

            q = request.GET['q']

            sea = Product.objects.filter(Q(title__icontains=q) | Q(description__icontains=q))

            newlevcat = []

            newcat= []

            for sea_items in sea:
                if sea_items.product_levcategory not in newlevcat:
                    newlevcat.append(sea_items.product_levcategory)
                    newcat.append(sea_items.product_levcategory.product_category)

            rrr = sea.only('product_levcategory').filter(Q(title__icontains=q) | Q(description__icontains=q)).order_by('product_levcategory')

            mlev=[]

            for yyy in rrr:
                if yyy.product_levcategory not in mlev:
                    mlev.append(yyy.product_levcategory)

            #for ncat in mlev:
            #    print ncat.amount(ncat,rrr)

            res = []

            #for mcat in mlev:
            #    mcat = mcat + '- ' +str(mcat.amount(mcat,rrr))

            #print mlev,res

            #for ooo in mlev:
            #    lll.append(ooo.product_set.count)

            #categorys = ProductCategory.objects.all()

            #subcategorys = ProductLevCategory.objects.all().select_related('product_category')

            #products = Product.objects.all().select_related('product_levcategory').order_by('product_levcategory')

            products_list = sea

            paginator = Paginator(products_list, 9)

            page = request.GET.get('page')

            try:
                page_obj = paginator.page(page)
            except PageNotAnInteger:
                page_obj = paginator.page(1)
            except EmptyPage:
                page_obj = paginator.page(paginator.num_pages)
            vars = dict(
                page_obj=page_obj,subcategorys=mlev,categorys= newcat,items=sea,products=rrr,
                querysea=q,sublev='No results',cartm =get_good(request),carta=get_totalprice(request)
               )

            return render_to_response('products/search_form.html', vars, context_instance=RequestContext(request))



            #context =  {'items': sea,'products': products, 'sublev':'No results','subcategorys': subcategorys, 'categorys': categorys , 'querysea' : q}

            #return render(request, 'products/search_form.html',context)
        else:
            return render(request, 'products/search_form.html')


class DetailView(generic.DetailView):
    model = Product
    template_name = 'products/detail.html'

def get_review(request):
    if request.user.is_authenticated():
        # Provide using user-based review for authenticated users
        try:
            review = Review.objects.get(user=request.user)

        except Review.DoesNotExist:
            review = Review(user=request.user)
            review.save()
    else:
        # Provide using session-based cart for nonauthenticated users
        request.session.save()
        current_session = Session.objects.get(pk = request.session.session_key)
        try:
            review = Review.objects.get(session=current_session)
        except Review.DoesNotExist:
            review = Review(session=current_session)
            review.save()
    return review

def get_comments(request,pk):

    p = get_object_or_404(Product, pk=pk)

    try:
        product_comments = ''

        comments = ProductComments.objects.filter(idprod = p.pk)

        for comm in comments:

            product_comments = product_comments +'\n' + str(comm.auth_username) + ':' +(comm.product_comments_text)

        print product_comments

        return product_comments


    except ProductComments.DoesNotExist:

        comments=0

        product_comments=''

        return product_comments




def showdetail (request, pk):

    procom = get_comments(request,pk=pk)

    procomments = ProductComments()

    procomments.review = get_review(request)

    if procomments.review.user:

        showres = 1
    else:
        showres = 0

    try:

        p = get_object_or_404(Product, pk=pk)

        z = get_object_or_404(ProductLevCategory, levslug=p.product_levcategory.levslug)

        levelcategory = z

        k = get_object_or_404(ProductCategory, category_product_name = p.product_levcategory.product_category.category_product_name)

        category = k

        mlevslug = z.levslug

        categorys = ProductCategory.objects.all()

        subcategorys = ProductLevCategory.objects.all().select_related('product_category')

        products = Product.objects.all().select_related('product_levcategory').order_by('product_levcategory')

        good_request = get_good(request)

        context =  {'showres':showres,'comments': procom ,'product': p,'catname': category ,'sublev': levelcategory ,'mlevslug': mlevslug, 'products': products,'subcategorys': subcategorys, 'categorys': categorys,'cartm':good_request,'carta':get_totalprice(request) }

        return render(request, 'products/detail.html',context)

    except (KeyError, ProductComments.DoesNotExist):

        return render(request, 'products/detail.html',context)


def rating (request, product_id):

    procom = get_comments(request,pk=product_id)

    p = get_object_or_404(Product, pk=product_id)

    z = get_object_or_404(ProductLevCategory, levslug=p.product_levcategory.levslug)

    levelcategory = z

    k = get_object_or_404(ProductCategory, category_product_name = p.product_levcategory.product_category.category_product_name)

    category = k

    mlevslug = z.levslug

    categorys = ProductCategory.objects.all()

    subcategorys = ProductLevCategory.objects.all().select_related('product_category')

    products = Product.objects.all().select_related('product_levcategory').order_by('product_levcategory')

    good_request = get_good(request)

    context =  {'cartm': good_request ,'product': p,'comments': procom,'catname': category ,'sublev': levelcategory ,'mlevslug': mlevslug, 'products': products,'subcategorys': subcategorys, 'categorys': categorys }

    try:
        selected_choice = request.POST['ans_num']

        input_text = request.POST['comment_text']

        z = Decimal(selected_choice)

        curr_rating = Decimal(p.product_rating)

        curr_votes = Decimal(p.product_vote)

        if 0 == curr_votes:

            p.product_rating = z

            p.product_vote = curr_votes+1

        else:

            p.product_rating =  Decimal(curr_votes*curr_rating + z) / Decimal(curr_votes + 1)

            p.product_vote = curr_votes + 1

            procomments = ProductComments()

            procomments.review = get_review(request)


            procomments.product_comments_text= ' -> ' + str(datetime.now().strftime(" %H-%M %d/%m/%y")) +'           ' +  input_text

            procomments.idprod =  Product.objects.get(pk=p.pk)

            if  procomments.review.user !=0:

                procomments.auth_username=procomments.review.user

                procomments.save()

                p.save()


    except (KeyError, Product.DoesNotExist):

         # Redisplay the question voting form.ans



        return render(request, 'products/detail.html', {'product': p,'subcategorys': subcategorys,'mlevslug': mlevslug,'sublev': levelcategory, 'categorys': categorys,


             'error_message': "  ",

         })

    else:


        p.save()

         # Always return an HttpResponseRedirect after successfully dealing

         # with POST data. This prevents data from being posted twice if a

         # user hits the Back button.products/views.py:135

        return HttpResponseRedirect(reverse('products:detail', args=(p.id,)))



class SearchView(generic.DetailView):
    model = Product
    template_name = 'products/search_form.html'

