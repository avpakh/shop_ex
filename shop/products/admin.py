from django.contrib import admin

# Register your models here.
from .models import Product
from .models import ProductCategory
from .models import ProductLevCategory
from .models import ProducerProduct
from .models import ProductRating
from .models import ProductTags
from .models import ProductComments
from sorl.thumbnail.admin import AdminImageMixin

class MyModelAdmin(AdminImageMixin,admin.ModelAdmin):
	pass

class ProductAdmin(admin.ModelAdmin):
	date_hierarchy = 'timestamp' #updated
	search_fields = ['title', 'description']
	list_display = ['title', 'price', 'active', 'updated']
	list_editable = ['price', 'active']
	list_filter = ['price', 'active','product_levcategory']
	readonly_fields = ['updated', 'timestamp']
	prepopulated_fields = {"slug": ("title",)}
	class Meta:
		model = Product
	pass

class ProductCategoryAdmin(admin.ModelAdmin):
	search_fields = ['category_product_name']
	list_display = ['category_product_name' ]
	list_filter = ['category_product_name']
	prepopulated_fields = {"catslug": ("category_product_name",)}

	class Meta:
		model = ProductCategory
	pass

class ProductLevCategoryAdmin(admin.ModelAdmin):
	search_fields = ['levcategory_product_name']
	list_display = ['levcategory_product_name' ]
	list_filter = ['levcategory_product_name','product_category']
	prepopulated_fields = {"levslug": ("levcategory_product_name",)}

	class Meta:
		model = ProductLevCategory
	pass

class ProducerProductAdmin(admin.ModelAdmin):
	search_fields = ['producer_product_name']
	list_display = ['producer_product_name' ]
	list_filter = ['producer_product_name']

	class Meta:
		model = ProducerProduct
	pass

class ProductRatingAdmin(admin.ModelAdmin):
	list_display = ['product_rating' ]
	list_filter = ['product_rating']

	class Meta:
		model = ProductRating
	pass


class ProductTagsAdmin(admin.ModelAdmin):
	list_display = ['product_tags_text' ]
	list_filter = ['product_tags_text']

	class Meta:
		model = ProductTags
	pass


class ProductCommentsAdmin(admin.ModelAdmin):
	list_display = ['product_comments_text','review' ]


	class Meta:
		model = ProductTags
	pass




admin.site.register(Product, ProductAdmin)
admin.site.register(ProductCategory, ProductCategoryAdmin)
admin.site.register(ProductLevCategory, ProductLevCategoryAdmin)
admin.site.register(ProducerProduct, ProducerProductAdmin)
admin.site.register(ProductRating, ProductRatingAdmin)
admin.site.register(ProductTags, ProductTagsAdmin)
admin.site.register(ProductComments, ProductCommentsAdmin)