# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.sessions.models import Session

from .models import Cart
from .models import Order
from .models import Good
from .forms import IndexForm
from .forms import AmmountForm
from products.models import Product

def get_cart(request):
    if request.user.is_authenticated():
        # Provide using user-based cart for authenticated users
        try:
            cart = Cart.objects.get(user=request.user)
        except Cart.DoesNotExist:
            cart = Cart(user=request.user)
            cart.save()
    else:
        # Provide using session-based cart for nonauthenticated users
        request.session.save()
        current_session = Session.objects.get(pk = request.session.session_key)
        try:
            cart = Cart.objects.get(session=current_session)
        except Cart.DoesNotExist:
            cart = Cart(session=current_session)
            cart.save()
    return cart

def remove(request, order_id, good_id):
    cart = get_cart(request)
    try:
        order = Order.objects.get(cart=cart, id=order_id)
    except Order.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Заказ не найден'})
    try:
        good = Good.objects.get(id=good_id, order=order)
    except Good.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Товар не найден'})
    good.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def add(request, product_id):
    cart = get_cart(request)
    try:
        good_type = Product.objects.get(id=product_id)
    except Product.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Товар не найден'})
    try:
        order = Order.objects.get(cart=cart, status='unsubmited')
    except Order.DoesNotExist:
        order = Order()
        order.cart = get_cart(request)
        order.save()
    try:
        good = order.good_set.get(good_type = good_type)
        if good:
            return render(request, 'cart/err.html', {'err': u'Товар уже добавлен в корзину'})
    except Good.DoesNotExist:
        good = Good()
        good.good_type = good_type
        good.order = order
        good.fixed_price = good_type.price
        good.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def index(request):
    cart = get_cart(request)
    try:
        order = Order.objects.get(cart=cart, status=u'не отправлен')
    except Order.DoesNotExist:
        order = None
    if order:
        if request.method == 'POST':
            indexform = IndexForm(request.POST)
            if indexform.is_valid():
                order.self_delivery = indexform.cleaned_data['self_delivery']
                order.delivery_adress = indexform.cleaned_data['delivery_adress']
                order.delivery_time = indexform.cleaned_data['delivery_time']
                order.contact_phone = indexform.cleaned_data['contact_phone']
                order.status = u'ожидание'
                order.save()
                return render(request, 'cart/thanks.html')
        else:
            indexform = IndexForm()
    else:
        return render(request, 'cart/index.html')
    return render(request, 'cart/index.html', {'order': order, 'indexform': indexform})

def edit(request, order_id):
    cart = get_cart(request)
    try:
        order = Order.objects.get(cart=cart, id=order_id)
    except Order.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Заказ не найден'})
    if order.status not in ('waiting', "cancelled by user"):
        return render(request, 'cart/err.html', {'err': u'Нельзя отменить заказ. Свяжитесь с менеджером'})
    if request.method == 'POST':
        editform = IndexForm(request.POST)
        if editform.is_valid():
            order.self_delivery = editform.cleaned_data['self_delivery']
            order.delivery_adress = editform.cleaned_data['delivery_adress']
            order.delivery_time = editform.cleaned_data['delivery_time']
            order.contact_phone = editform.cleaned_data['contact_phone']
            order.save()
            return HttpResponseRedirect('/cart/history')
    else:
        editform = IndexForm(initial={'self_delivery': order.self_delivery, \
                                    'delivery_adress': order.delivery_adress, \
                                    'delivery_time': order.delivery_time, \
                                    'contact_phone': order.contact_phone})
    return render(request, 'cookiecart/edit.html', {'order': order, 'editform': editform})

def changeammount(request, order_id, good_id):
    cart = get_cart(request)
    try:
        order = Order.objects.get(cart=cart, id=order_id)
    except Order.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Заказ не найден'})
    try:
        good = Good.objects.get(order=order, id=good_id)
    except Good.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Товар не найден'})
    if request.method == 'POST':
        ammountform = AmmountForm(request.POST)
        if ammountform.is_valid():
            good.ammount = ammountform.cleaned_data['ammount']
            good.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def history(request):
    cart = get_cart(request)
    context = {'orders': cart.order_set.all()}
    return render(request, 'cart/history.html', context)

def status(request, order_id, status_id):
    cart = get_cart(request)
    try:
        order = Order.objects.get(cart=cart, id=order_id)
    except Order.DoesNotExist:
        return render(request, 'cart/err.html', {'err': u'Заказ не найден'})
    if order.status == u'отменен пользователем' and status_id == '1':
        order.status = u'ожидание'
    elif order.status == u'ожидание' and status_id == '4':
        order.status = u'отменен пользователем'
    order.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
