# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.sessions.models import Session
from django.utils import timezone

from accounts.models import AuthUser
from products.models import Product
from management.models import ManagerUser

class Cart(models.Model):
    user = models.OneToOneField(AuthUser, null=True)
    session = models.OneToOneField(Session, null=True)

    def __unicode__(self):
        if self.user:
            return u'{} cart'.format(self.user)
        elif self.session:
            return u'{} cart'.format(self.session.pk)
        else:
            return Null

class Order(models.Model):
    STATUS_CHOICES = (
        (u'не отправлен', u'не отправлен'),
        (u'ожидание', u'ожидание'),
        (u'обработка', u'обработка'),
        (u'отменен менеджером', u'отменен менеджером'),
        (u'отменен пользователем', u'отменен пользователем'),
        (u'завершен', u'завершен'))
    cart = models.ForeignKey(Cart)
    manager_user = models.ForeignKey(ManagerUser, null=True)
    timestamp = models.DateTimeField(default=timezone.now())
    status = models.CharField(max_length=20, choices = STATUS_CHOICES, default=u'не отправлен')
    self_delivery = models.BooleanField(default=False)
    delivery_adress = models.CharField(max_length=100, null=True)
    delivery_time = models.CharField(max_length=50, null=True)
    contact_phone = models.CharField(max_length=15, null=True)

    def __unicode__(self):
        return u'Order in {}'.format(self.cart)

    def get_total(self):
        total = 0
        for good in self.good_set.all():
            total += good.fixed_price*good.ammount
        return total

class Good(models.Model):
    good_type = models.ForeignKey(Product)
    order = models.ForeignKey(Order)
    ammount = models.IntegerField(default=1)
    fixed_price = models.DecimalField(decimal_places=2, max_digits=100)
