from django.contrib import admin

from .models import Cart, Order, Good

class CartAdmin(admin.ModelAdmin):
    search_fields = ['user', 'session']
    list_display = ['id', 'user', 'session']
    list_filter = ['user', 'session']
    class Meta:
        model = Cart
    pass

class OrderAdmin(admin.ModelAdmin):
    search_fields = ['cart']
    list_display = ['id', 'cart', 'timestamp', 'status', \
                    'delivery_adress', 'delivery_time', 'contact_phone']
    list_filter = ['cart']
    class Meta:
        model = Order
    pass

class GoodAdmin(admin.ModelAdmin):
    search_fields = ['good_type']
    list_display = ['id', 'good_type', 'ammount', 'fixed_price']
    list_filter = ['good_type']
    class Meta:
        model = Good
    pass

admin.site.register(Cart, CartAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Good, GoodAdmin)
