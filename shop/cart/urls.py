from django.conf.urls import include, url


urlpatterns = [
        url(r'^add/(?P<product_id>[0-9]+)/$', 'cart.views.add', name='add'),
        url(r'^remove/(?P<order_id>[0-9]+)/(?P<good_id>[0-9]+)/$', 'cart.views.remove', name='remove'),
        url(r'^edit/(?P<order_id>[0-9]+)/$', 'cart.views.edit', name='edit'),
        url(r'^status/(?P<order_id>[0-9]+)/(?P<status_id>[0-5]+)/$', 'cart.views.status', name='status'),
        url(r'^changeammount/(?P<order_id>[0-9]+)/(?P<good_id>[0-9]+)/$', 'cart.views.changeammount', name='changeammount'),
        url(r'^$', 'cart.views.index', name='index'),
        url(r'^history/$', 'cart.views.history', name='history'),
]
