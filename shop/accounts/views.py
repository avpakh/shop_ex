from django.shortcuts import render, render_to_response, get_object_or_404
from forms import RegistrationForm, UpdateProfileForm,AuthenticationForm
import hashlib
import datetime
import random
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse
from models import UserProfile
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
# Create your views here.
from django.contrib.auth import login as django_login, authenticate, logout as django_logout
from django.template import RequestContext



def register_user(request):

    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            #new_user.username = form.cleaned_data['username']
            #new_user.email = form.cleaned_data['email']
            #new_user.phone = form.cleaned_data['phone']

            if new_user.email:
                salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
                new_user.activation_key = hashlib.sha1(salt+new_user.email).hexdigest()
                new_user.key_expires = datetime.datetime.today() + datetime.timedelta(2)
                new_user.save()
                # Send email with activation key
                email_subject = 'Account confirmation'
                email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
                48hours http://127.0.0.1:8000/accounts/confirm/%s" % (new_user.username, new_user.activation_key)

                send_mail(email_subject, email_body, 'sendmail@z0.by',
                    [new_user.email], fail_silently=False)

                return HttpResponseRedirect('/accounts/registersuccess')

            elif new_user.phone:
                new_user.save()
                return HttpResponseRedirect('/accounts')
            else:
                return HttpResponseRedirect('/')



    else:
        form = RegistrationForm()

    return render(request, 'accounts/register.html', {'form': form})

def register_confirm(request, activation_key):
    #check if user is already logged in and if he is redirect him to some other url, e.g. home
    if request.user.is_authenticated():
        HttpResponseRedirect('/')

    # check if there is UserProfile which matches the activation key (if not then display 404)
    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

    #check if the activation key has expired, if it hase then render confirm_expired.html
    if user_profile.key_expires < timezone.now():
        return render_to_response('accounts/confirm_expired.html')
    #if the key hasn't expired save user and set him as active and render some template to confirm activation
    user = user_profile.user
    user.is_active = True
    user.save()
    return render_to_response('accounts/confirm.html')

@login_required(login_url='/accounts/login/')
def edit_profile(request):
    user_profile =  request.user
    #form = UpdateProfileForm(instance=user_profile)
    if request.POST:
        form = UpdateProfileForm(request.POST,instance=user_profile)
        if form.is_valid():

            form.save()
            return HttpResponseRedirect('/accounts/edit')
        else:
            return HttpResponseRedirect('/')

    form = UpdateProfileForm(instance=user_profile)

    return render(request, 'accounts/edit.html', {'form': form})


def login_user(request):
    """
    Log in view
    """
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST['username'], password=request.POST['password'])
            if user is not None:
                if user.is_active:
                    django_login(request, user)
                    return HttpResponseRedirect('/')



    else:
        form = AuthenticationForm()
    return render_to_response('accounts/login.html', {
        'form': form,
    }, context_instance=RequestContext(request))

def register_success(request):

     return render_to_response('accounts/register_success.html', {}, context_instance=RequestContext(request))

def logout_user(request):
    logout(request)
    return HttpResponseRedirect('/')