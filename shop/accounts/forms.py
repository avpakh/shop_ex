# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
from accounts.models import AuthUser,UserProfile
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from django.contrib.auth.forms import ReadOnlyPasswordHashField


class AuthUserCreationForm(UserCreationForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirmation', widget=forms.PasswordInput)

    class Meta(UserCreationForm.Meta):
        model = AuthUser
        fields = ('email', 'username')

    def clean_username(self):
        username = self.cleaned_data['username']

        try:
            AuthUser._default_manager.get(username=username)
        except AuthUser.DoesNotExist:
            return username
        raise forms.ValidationError(_('Такой пользователь уже существует'), code='duplicate')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords do not match.')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

class AuthUserChangeForm(UserChangeForm):
    password = ReadOnlyPasswordHashField(label='password', help_text='There is no way to see this password.')

    class Meta(UserChangeForm.Meta):
        model = AuthUser
        fields = ('email', 'password', 'is_active', 'is_staff', 'is_superuser', 'user_permissions')

    def clean_password(self):
        return self.initial['password']

#class RegistrationForm(AuthUserCreationForm):
#
#    class Meta:
#        model = UserProfile
#        fields = ( 'username', 'password1', 'password2')
        #clean email field

#    def save(self, commit=True):
#        user = super(RegistrationForm, self).save(commit=False)
        #user.username = self.cleaned_data['username']
#        if commit:
#            user.is_active = False # not active until he opens activation link
#            user.save()

#        return user

class RegistrationForm(forms.Form):
    username = forms.CharField(required=True, label="Email или телефон")
    password1 = forms.CharField(required=True, label='Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(required=True, label='Подтверждение пароля', widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username']

        try:
            AuthUser._default_manager.get(username=username)
        except AuthUser.DoesNotExist:
            return username
        raise forms.ValidationError(('Такой пользователь уже существует'), code='duplicate')
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(('Пароли не совпадают'), code='password')
        return password2

    def save(self, commit=False):
        data = self.cleaned_data
        if data['username']  and data['password1']:
            username = data['username']
            password = data['password1']
            customer = UserProfile.objects.create_user(username = username, password = password, is_active = False)
            customer.save()
        return customer

class UpdateProfileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.prof = kwargs.get('instance', None)
        initial = {'username': self.prof.username, 'email': self.prof.email,'phone': self.prof.phone, 'first_name':self.prof.first_name,'last_name': self.prof.last_name}
        kwargs['initial'] = initial
        super(UpdateProfileForm, self).__init__( *args, **kwargs)
    class Meta:
        model = UserProfile
        fields = ('username', 'email', 'phone', 'first_name', 'last_name')


class AuthenticationForm(forms.Form):
    """
    Login form
    """
    username = forms.CharField(required=True, label="Email или телефон")
    password = forms.CharField(widget=forms.widgets.PasswordInput, label="Пароль")

    class Meta:
        fields = ['username', 'password']


